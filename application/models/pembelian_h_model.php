<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class pembelian_h_model extends CI_Model
{
	//panggil nama table
	private $_table_header = "pembelian_h";
	private $_table_detail = "pembelian_d";
	
	public function rules()
	{
		return
		[
			[
				'field' => 'no_trans',
				'label'	=> 'no trans',
				'rules' => 'required|max_length[10]',
				'errors' =>[
					'required' => 'no transaksi tidak Boleh Kosong',
					'max_length' => 'no transaksi tdak Boleh Lebih dari 10 karakter',
				],
			],
			[
				'field' => 'kode_supplier',
				'label'	=> 'kode supplier',
				'rules' => 'required',
				'errors' =>[
					'required' => 'nama tidak Boleh Kosong',
				],
			]
		];
	}
	public function rules1()
	{
		return
		[
			[
				'field' => 'kode_barang',
				'label'	=> 'kode barang',
				'rules' => 'required|max_length[5]',
				'errors' =>[
					'required' => 'kode barang tidak Boleh Kosong',
					'max_length' => 'kode barang tdak Boleh Lebih dari 10 karakter',
				],
			],
			[
				'field' => 'qty',
				'label'	=> 'qty',
				'rules' => 'required',
				'errors' =>[
					'required' => 'qty tidak Boleh Kosong',
				],
			],
			[
				'field' => 'harga',
				'label'	=> 'harga',
				'rules' => 'required',
				'errors' =>[
					'required' => 'harga tidak Boleh Kosong',
				],
			]
		];
	}
	
	public function tampilDataPembelian_h()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table_header)->result();
	}

	//public function tampilDatapembelian_h2()
	//{
		//menggunakan query
		//$query = $this->db->query("SELECT * FROM" . $this->_table_header . "where flag = 1");
		//return $query->result();
	//}
	

	public function tampilDataPembelian_h3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('id_pembelian_h', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$data['no_trans']			= $this->input->post('no_trans');
		$data['kode_supplier']		= $this->input->post('kode_supplier');
		$data['tanggal']			= date ('y-m-d');
		$data['approved']			= 1;
		$data['flag']				= 1;
		$this->db->insert($this->_table_header, $data);


	}
	public function idTransaksiTerakhir()
	{
		$query	= $this->db->query("SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_pembelian_h DESC LIMIT 0,1 ");
		$data_id = $query->result();

		foreach ($data_id as $data ) {
			# code...
			$last_id = $data->id_pembelian_h;
		}
		return $last_id;
	}
	public function tampilDataPembelianDetail($id)
	{
		  $query	= $this->db->query(
            "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_pembelian_h = '$id'"
        );
        return $query->result();


	}
	 public function savePembelianDetail($id)
    {
        $qty    = $this->input->post('qty');
        $harga  = $this->input->post('harga');

        $data['id_pembelian_h'] = $id;
        $data['kode_barang']    = $this->input->post('kode_barang');
        $data['qty']            = $qty;
        $data['harga']          = $harga;
        $data['jumlah']         = $qty * $harga;
        $data['flag']           = 1;

        $this->db->insert($this->_table_detail, $data);
    }
     public function tampilDataReport1($tgl_awal, $tgl_akhir)
	{
		//$tgl = $this->load->post('pembelian_h.tanggal');
		//$this->input->post($tgl);
		// echo "<pre>";
		// print_r($tgl_awal); die();
		// echo "</pre>";

		$this->db->select("ph.id_pembelian_h, ph.no_trans, ph.tanggal, COUNT(pd.kode_barang) as total, SUM(pd.qty) as qty, sum(pd.jumlah) as jumlah");
		$this->db->from('pembelian_h ph');
		$this->db->join('pembelian_d pd', 'pd.id_pembelian_h = ph.id_pembelian_h');
		$this->db->where("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
		$this->db->group_by('ph.no_trans');
		$query = $this->db->get();
		return $query->result();

		
	}
    public function tampilDataPembelianPagination($perpage, $uri, $data_pencarian)
	{
		$this->db->select('*');
		if (!empty($data_pencarian)) {
			# code...
			$this->db->like('no_trans', $data_pencarian);
		}
		$this->db->order_by('id_pembelian_h', 'asc');

		$get_data = $this->db->get($this->_table_header,  $perpage, $uri);
		if ($get_data->num_rows()>0) {
			# code...
			return $get_data->result();
		}else{
			return null;
		}
	}
	public function tombolpagination($data_pencarian)
	{
		$this->db->like('no_trans', $data_pencarian);
		$this->db->from($this->_table_header);
		$hasil = $this->db->count_all_results();

		$pagination['base_url']		= base_url().'pembelian_h/listpembelian_h/load/';
		$pagination['total_rows']	= $hasil;
		$pagination['per_page']		= "3";
		$pagination['uri_segment']	= 4;
		$pagination['num_links']	= 2;

		$pagination['full_tag_open']	= '<div class="pagination">';
		$pagination['full_tag_close']	= '</div>';

		$pagination['first_link']		= 'First Page';
		$pagination['first_tag_open']	= '<span class="firstlink">';
		$pagination['first_tag_close']	= '</span>';

		$pagination['last_link']		= 'Last Page';
		$pagination['last_tag_open']	= '<span class="lastlink">';
		$pagination['last_tag_close']	= '</span>';

		$pagination['next_link']		= 'Next Page';
		$pagination['next_tag_open']	= '<span class="nextlink">';
		$pagination['next_tag_close']	= '</span>';

		$pagination['prev_link']		= 'Prev Page';
		$pagination['prev_tag_open']	= '<span class="prevlink">';
		$pagination['prev_tag_close']	= '</span>';

		$pagination['cur_tag_open']		= '<div class="curlink">';
		$pagination['cur_tag_close']	= '</div>';

		$pagination['num_tag_open']		= '<div class="numlink">';
		$pagination['num_tag_close']	= '</div>';

		$this->pagination->initialize($pagination);

		$hasil_pagination = $this->tampilDataPembelianPagination($pagination['per_page'],
			$this->uri->segment(4), $data_pencarian);

		return $hasil_pagination;



	}


}